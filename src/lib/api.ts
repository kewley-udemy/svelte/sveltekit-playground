import { delay } from '$lib/util';
import { EMPTY_POKEMON, type Pokemon, type PokemonApiResponse, type PokemonData } from '$lib/index';

export async function loadPokemonData(
  name: string,
  setHeaders: (headers: Record<string, string>) => void,
  delayInMillis: number = 0
): Promise<PokemonData> {
  await delay(delayInMillis);
  const response = await fetch(`/api/pokemon?name=${name}`);
  if (response.ok) {
    setHeaders({
      // @ts-expect-error age may be present
      age: response.headers.get('age'),
      // @ts-expect-error cache-control may be present
      'cache-control': response.headers.get('cache-control')
    });
    const pokemon = (await response.json()) as PokemonApiResponse;
    return {
      ...EMPTY_POKEMON,
      name: pokemon.name,
      cry: pokemon.cries.latest,
      id: pokemon.id,
      sprite: pokemon.sprites.front_default,
      sprite_shiny: pokemon.sprites.front_default,
      types: pokemon.types.map((t) => t.type.name)
    };
  }
  return { ...EMPTY_POKEMON };
}

export async function loadPokemonList(limit: number = 151, offset: number = 0): Promise<Pokemon[]> {
  console.info(limit, offset);
  return [];
}
