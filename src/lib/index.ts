export { default as PokemonDetails } from './PokemonDetails.svelte';

export type PokemonApiResponse = {
  name: string;
  id: number;
  cries: { latest: string; legacy: string };
  sprites: { front_default: string; front_shiny: string };
  types: { slot: number; type: { name: string; url: string } }[];
};

export type Pokemon = {
  name: string;
  url: string;
};

export type PokemonData = {
  name: string;
  id: number;
  cry: string;
  sprite: string;
  types: string[];
  sprite_shiny: string;
};

export const EMPTY_POKEMON: PokemonData = {
  name: 'Missing No.',
  id: -1,
  cry: '',
  sprite: '',
  types: [],
  sprite_shiny: ''
};
