import { error, json } from '@sveltejs/kit';

const BASE_API = 'https://pokeapi.co/api/v2';

const ENDPOINT = 'pokemon';

export async function GET({ url }) {
  console.info('In here', url);
  const pokemonName = url.searchParams.get('name');
  if (pokemonName == null || pokemonName.trim() === '') {
    error(400, 'name is a required parameter');
  }

  const query = `${BASE_API}/${ENDPOINT}/${pokemonName}`;
  console.info('Querying', query);
  const response = await fetch(query);
  if (response.ok) {
    const age = response.headers.get('age');
    const cacheControl = response.headers.get('cache-control');
    const responseHeaders: Record<string, string> = {};
    if (age) {
      responseHeaders['age'] = age;
    }
    if (cacheControl) {
      responseHeaders['cache-control'] = cacheControl;
    }

    const pokemonData = await response.json();
    console.info(pokemonData);
    return json(pokemonData, {
      headers: responseHeaders
    });
  }
  console.error('Unable to fetch pokemon data', response.status, response.statusText);
  error(500, 'unknown error');
}
