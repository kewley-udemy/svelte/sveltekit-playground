import { loadPokemonData } from '$lib/api';

// Ensure this is client only due to use of /api here
export const ssr = false;

export async function load({ params, setHeaders }) {
  return await loadPokemonData(params.slug, setHeaders);
}
