export const actions = {
  login: async ({ request, cookies }) => {
    const data = await request.formData();
    console.info(data);
    // Fake login
    cookies.set('sessionId', crypto.randomUUID(), { path: '/' });
    return { success: true };
  },
  comment: async ({ request }) => {
    const data = await request.formData();
    console.info(data);
    return { success: true };
  }
};
