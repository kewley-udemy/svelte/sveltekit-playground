import { loadPokemonData } from '$lib/api';

export const ssr = false;

export async function load({ params, setHeaders, depends }) {
  depends('app:pokemon');
  return {
    pokemon: loadPokemonData(params.slug, setHeaders, 2000)
  };
}
