import type { SSTConfig } from 'sst';
import { SvelteKitSite } from 'sst/constructs';

export default {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  config(_input) {
    return {
      name: 'sveltekit-playground',
      region: 'us-east-1'
    };
  },
  stacks(app) {
    app.stack(function Site({ stack }) {
      const site = new SvelteKitSite(stack, 'site');
      stack.addOutputs({
        url: site.url
      });
    });
  }
} satisfies SSTConfig;
